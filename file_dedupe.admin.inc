<?php

/**
 * Admin form for the deduplication process.
 */
function file_dedupe_admin_form($form, &$form_state) {

  return confirm_form(
    $form,
    t('Start de-duplication process'),
    'admin/config/media/file-system',
    t('This could result in data loss!'),
    t('Begin de-duplication process'));
}

/**
 * Form submit callback.
 *
 * @see file_dedupe_admin_form()
 */
function file_dedupe_admin_form_submit($form, &$form_state) {
  // Set up a batch, and start that up.
  $batch = array();

  $batch['operations'][] = array('file_dedupe_batch_find_file_fields', array());

  $batch['operations'][] = array('file_dedupe_batch_identify_potentials', array());

  $batch['operations'][] = array('file_dedupe_batch_hash_potentials', array());

  $batch['operations'][] = array('file_dedupe_batch_identify_duplicates', array());

  $batch['file'] = drupal_get_path('module', 'file_dedupe') . '/file_dedupe.admin.inc';

  batch_set($batch);
}

/**
 * Identify potential dupes.
 */
function file_dedupe_batch_identify_potentials(&$context) {
  // Get the fields that we should care about in the files_managed table.
  $schema = drupal_get_schema('file_managed');
  $fields = array();
  foreach ($schema['fields'] as $field => $v) {
    $fields[] = $field;
  }

  $fields = array_diff($fields, file_dedupe_columns_difference_columns());


  // Construct the DB query.
  $query = db_select('file_managed', 'file1');
  $query->fields('file1', array('fid'));
  $query->addField('file2', 'fid', 'dup_fid');
  $query->innerJoin('file_managed', 'file2');
  // We'll get back lots of results for the same file, because of the join.
  $query->distinct();

  $query->where('file1.fid != file2.fid');

  // Now add all the duplicate finding conditions.
  foreach ($fields as $field) {
    $query->where('file1.' . db_escape_field($field) . ' = file2.' . db_escape_field($field));
  }

  $results = $query->execute();
  $potentials = array();
  foreach ($results as $row) {
    $potentials[$row->fid][] = $row->dup_fid;
  }

  $context['results']['potentials'] = $potentials;

}

function file_dedupe_batch_hash_potentials(&$context) {


  if (!isset($context['results']['hashes'])) {
    $context['results']['hashes'] = array();
  }

  // Are there more hashes to compute.
  $to_compute = array_diff(array_keys($context['results']['potentials']), array_keys($context['results']['hashes']));

  if (!empty($to_compute)) {
    $next = reset($to_compute);
    $file = file_load($next);
    $context['results']['hashes'][$next] = sha1_file(drupal_realpath($file->uri));
    $context['finished'] = count($context['results']['hashes']) / count($context['results']['potentials']);
  }


//dpm($context);

}

function file_dedupe_batch_identify_duplicates(&$context) {
  // Loop over the potential duplicates, and see if they really are.
  foreach ($context['results']['potentials'] as $fid => $possibles) {
    $our_hash = $context['results']['hashes'][$fid];
    if (empty($our_hash)) {
      // Couldn't hash this file.
      unset($context['results']['potentials'][$fid]);
    }
    foreach ($possibles as $k => $possible_fid) {
      if ($context['results']['hashes'][$possible_fid] != $our_hash) {
        // This isn't a duplicate, remove from this duplicate set.
        unset($context['results']['potentials'][$fid][$k]);
      }
    }
  }

  // At this point, we can reduce our sets of duplicates down a lot.
  $fids_processed = array();
  foreach ($context['results']['potentials'] as $fid => $possibles) {
    if (count($possibles) == 0) {
      unset($context['results']['potentials'][$fid]);
    }
    $fids_processed[] = $fid;
    foreach ($possibles as $k => $possible_fid) {
      // Each of these sets should also contain a set with us in it, unset that.
      if (!in_array($possible_fid, $fids_processed)) {
        unset($context['results']['potentials'][$possible_fid]);
      }
    }

  }


  if (empty($context['results']['potentials'])) {
    drupal_set_message(t('No duplicates found'));
  }
  else {
    // Create a new batch to process them.
    // Set up a batch, and start that up.
    $batch = array();

    foreach ($context['results']['fields'] as $field_name) {
      foreach ($context['results']['potentials'] as $fid => $dupes) {
        $batch['operations'][] = array('file_dedupe_batch_process_dupes', array($field_name, $fid, $dupes));
      }
    }

    $batch['file'] = drupal_get_path('module', 'file_dedupe') . '/file_dedupe.admin.inc';

    batch_set($batch);
  }
}

/**
 * Get a list of all the file fields on the site.
 */
function file_dedupe_batch_find_file_fields(&$context) {
  $instances = field_info_instances();
  $field_types = field_info_field_types();

  $context['results']['fields'] = array();

  foreach ($instances as $entity_type => $type_bundles) {
    foreach ($type_bundles as $bundle => $bundle_instances) {
      foreach ($bundle_instances as $field_name => $instance) {
        $field = field_info_field($field_name);

        if (($field['module'] == 'image') && ($field['type'] == 'image')) {
          $context['results']['fields'][$field_name] = $field_name;
        }
      }
    }
  }
}

/**
 * Process the given fid and duplicates down into a single file.
 *
 * What we have to do is find all the entities where the duplicates are used,
 * and change them to use the canonical fid.
 */
function file_dedupe_batch_process_dupes($field_name, $fid, $dupes, &$context) {
  if (empty($context['sandbox']['entities'])) {
    // Find the entities.
    $query = new EntityFieldQuery();
    $query->fieldCondition($field_name, 'fid', $dupes, 'IN');
    $context['sandbox']['entities'] = $query->execute();
  }

  // Iterate over the entities and update them one-by-one.
  if (!empty($context['sandbox']['entities'])) {
    // Get the first entity type, and start there.
    $entity_type = reset(array_keys($context['sandbox']['entities']));
    $entity_to_process = reset(array_keys($context['sandbox']['entities'][$entity_type]));
    unset($context['sandbox']['entities'][$entity_type][$entity_to_process]);
    // Is this the last entity to look at.
    if (empty($context['sandbox']['entities'][$entity_type])) {
      unset($context['sandbox']['entities'][$entity_type]);
    }
    $entity = reset(entity_load($entity_type, array($entity_to_process)));
    $wrapper = entity_metadata_wrapper($entity_type, $entity);
    $updated = FALSE;
    if (!empty($wrapper->$field_name)) {
      foreach ($wrapper->$field_name as $file) {
        if (in_array($file->raw(), $dupes, TRUE)) {
          // Fake the file usage deletion too.
          //file_field_delete_file($file->value(), $field_name, $entity_type, $entity_to_process);
          $file->set($fid);
          $updated = TRUE;

        }
      }
    }
    if ($updated) {
      // Special handling for the field_collection_item entity.
      if ($entity_type == 'field_collection_item') {
        $entity->save(TRUE);
      }
      else {
        $wrapper->save();
      }
    }
  }

  // If there's work to do, loop.
  if (!empty($context['sandbox']['entities'])) {
    $context['finished'] = 0.5;
  }

}

function file_dedupe_columns_difference_columns() {
  return array(
    'fid',
    'filename',
    'uri',
    'timestamp',
  );
}
